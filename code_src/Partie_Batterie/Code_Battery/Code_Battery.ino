#include <LBattery.h>

char buff[256];

int nivBat; //niveau de la batterie
int charge; //pour savoir si on est en train de charger la batterie ou non

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(13, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery level = %d", LBattery.level() ); 
  Serial.println(buff);
  nivBat=LBattery.level();
  Serial.println(nivBat);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
  

  if (nivBat<100) { //si le niveau de la batterie est inférieur à 100% on fait clignoter la LED
    clignoterLED();
    
  }
  
 delay(1000); 
}


void clignoterLED() { //fonction pour faire clignoter la LED
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(100);              // wait for a second
}

